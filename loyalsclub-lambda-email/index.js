const parseSnsMessage = require('./parse-sns-event');
const MODE = process.env.MODE;

const {
  sendDiscountEmail,
  addPartnerToList,
  addUserToList,
  sendPartnerWelcomeEmail,
  sendUserPartnerWelcomeEmail,
  sendUserWelcomeEmail,
  sendUserInvitationEmail
} = require('./email');

const { sendSesEmail } = require('./ses.js')

const EVENTS_MESSAGES = {
  "PARTNER_WELCOME_EMAIL": sendPartnerWelcomeEmail,
  "USER_WELCOME_EMAIL": sendUserWelcomeEmail,
  "USER_INVITATION_EMAIL": sendUserInvitationEmail,
  "ADD_PARTNER_TO_LIST": addPartnerToList,
  "ADD_USER_TO_LIST": addUserToList,
  "DISCOUNT_EMAIL": sendDiscountEmail,
  "USER_PARTNER_WELCOME_EMAIL": sendUserPartnerWelcomeEmail
}

exports.handler = (event) => {
  console.log('[DEBUG]', JSON.stringify(event))
  const parsedRecords = parseSnsMessage(event);
  const promises = [];

  parsedRecords.forEach(record => {
    const { message, params } = record;
    const action = params.SES === 'true' ? sendSesEmail : EVENTS_MESSAGES[message];

    if (MODE == 0) {
      console.log("Message received: ", message, params);
      return;
    }

    if (!action) {
      return console.error('INVALID_MESSAGE', JSON.stringify(record))
    };

    promises.push(action({ ...params, TEMPLATE: message }));
  });

  return Promise.all(promises);
};

