const request = require("request");
const SENDINBLUE_API_KEY = process.env.SENDINBLUE_API_KEY;

const EMAIL_CONFIG = {
  BASE_URL: 'https://api.sendinblue.com/v3/',
  USER_PARTNER_WELCOME_EMAIL: 105,
  PARTNER_WELCOME_TEMPLATE: 188,
  USER_WELCOME_TEMPLATE: 187,
  PARTNER_CONTACT_LIST: 20, //For massive email sending.
  USER_CONTACT_LIST: 19, //For massive email sending.
  DISCOUNT_EMAIL: 110,
  USER_INVITATION_EMAIL: 193
}

const _retrieveContact = async (params) => {
  const { EMAIL } = params;

  const payload = {
    method: 'GET',
    url: `contacts/${encodeURIComponent(EMAIL)}`,
  }

  return _sendinBlueRequestMaker(payload);
};

const _updateContact = (params) => {
  const { EMAIL, FIRST_NAME, LAST_NAME, listIds } = params;
  if(FIRST_NAME) params.NOMBRE = FIRST_NAME
  if(LAST_NAME) params.SURNAME = LAST_NAME;

  const payload = {
    method: 'PUT',
    url: `contacts/${encodeURIComponent(EMAIL)}`,
    body: {
      listIds,
      attributes: {
        NOMBRE: FIRST_NAME,
        SURNAME: LAST_NAME
      }
    }
  }

  return _sendinBlueRequestMaker(payload);
};

const _createContact = async (params) => {
  const { LAST_NAME, FIRST_NAME, EMAIL, STORE_NAME, listIds } = params;

  const payload = {
    method: 'POST',
    url: `contacts`,
    body: {
      listIds,
      email: EMAIL,
      attributes: {
        NOMBRE: `${FIRST_NAME}`,
        SURNAME: `${LAST_NAME}`,
        EMPRESA: STORE_NAME
      }
    },
  }

  return _sendinBlueRequestMaker(payload);
};

/** Actual request builder, payload needed.
 * method
 * url
 * body
 */
const _sendinBlueRequestMaker = (payload) => {
  const { method, url, body } = payload;

  let options = {
    method,
    url: `${EMAIL_CONFIG.BASE_URL + url}`,
    headers: {
      'api-key': SENDINBLUE_API_KEY
    }
  };

  if(method === 'POST' || method === 'PUT') {
    options = {
      ... options,
      body,
      json: true
    }
  }

  return new Promise((resolve) => {
    request(options, (error, response, body) => {
      if (error) throw new Error(error);
      resolve(body);
    });
  })
}

/** Send Disscount Email Function
 * Params Needed for template:
 * COUPON_NAME COUPON_CODE COUPON_REDEEMBY(OPT) COUPON_QR_IMAGE FIRST_NAME LAST_NAME STORE_PROFILE_IMAGE STORE_NAME
*/
const sendDiscountEmail = (params) => {
  const { LAST_NAME, FIRST_NAME, EMAIL_TO } = params;

  const payload = {
    method: 'POST',
    url: `smtp/email`,
    body: {
      to: [{'name': `${FIRST_NAME} ${LAST_NAME}`, 'email': EMAIL_TO}],
      templateId: EMAIL_CONFIG.DISCOUNT_EMAIL,
      params
    }
  }

  return _sendinBlueRequestMaker(payload);
}

/** Send User Partner Welcome Email Function
 * Params Needed for template:
 * EMAIL_TO FIRST_NAME LAST_NAME STORE_PROFILE_IMAGE STORE_NAME
*/
const sendUserPartnerWelcomeEmail = (params) => {
  const { LAST_NAME, FIRST_NAME, EMAIL_TO } = params;

  const payload = {
    method: 'POST',
    url: `smtp/email`,
    body: {
      to: [{'name': `${FIRST_NAME} ${LAST_NAME}`, 'email': EMAIL_TO}],
      templateId: EMAIL_CONFIG.USER_PARTNER_WELCOME_EMAIL,
      params
    }
  }

  return _sendinBlueRequestMaker(payload);
}

/** Send Partner Welcome Email Function
 * Params Needed for template:
 * EMAIL_TO FIRST_NAME LAST_NAME STORE_PROFILE_IMAGE STORE_NAME
*/
const sendPartnerWelcomeEmail = (params) => {
  const { LAST_NAME, FIRST_NAME, EMAIL_TO } = params;

  const payload = {
    method: 'POST',
    url: `smtp/email`,
    body: {
      to: [{'name': `${FIRST_NAME} ${LAST_NAME}`, 'email': EMAIL_TO}],
      templateId: EMAIL_CONFIG.PARTNER_WELCOME_TEMPLATE,
      params
    }
  }

  return _sendinBlueRequestMaker(payload);
}

/** Send User Welcome Email Function
 * Params Needed for template:
 * LAST_NAME FIRST_NAME EMAIL_TO ALIAS
*/
const sendUserWelcomeEmail = (params) => {
  const { LAST_NAME, FIRST_NAME, EMAIL_TO } = params;

  const payload = {
    method: 'POST',
    url: `smtp/email`,
    body: {
      to: [{'name': `${FIRST_NAME} ${LAST_NAME}`, 'email': EMAIL_TO}],
      templateId: EMAIL_CONFIG.USER_WELCOME_TEMPLATE,
      params
    }
  }

  return _sendinBlueRequestMaker(payload);
}

/*
** Send User Invitation Email Function
 * Params Needed for template:
 * LAST_NAME FIRST_NAME EMAIL_TO STORE_NAME STORE_PROFILE_IMAGE
*/
const sendUserInvitationEmail = (params) => {
  const { LAST_NAME, FIRST_NAME, EMAIL_TO } = params;

  const payload = {
    method: 'POST',
    url: `smtp/email`,
    body: {
      to: [{'name': `${FIRST_NAME} ${LAST_NAME}`, 'email': EMAIL_TO}],
      templateId: EMAIL_CONFIG.USER_INVITATION_EMAIL,
      params
    }
  }

  return _sendinBlueRequestMaker(payload);
}

const addPartnerToList = async (params) => {
  const rawContact = await _retrieveContact(params);
  const contact = JSON.parse(rawContact || {});

  if(contact && contact.email) {
    const updatePayload = {
      ... params,
      listIds: contact.listIds ? contact.listIds.concat(EMAIL_CONFIG.PARTNER_CONTACT_LIST) : [EMAIL_CONFIG.PARTNER_CONTACT_LIST]
    }

    return _updateContact(updatePayload);
  }

  const payload = {
    ... params,
    listIds: [ EMAIL_CONFIG.PARTNER_CONTACT_LIST ]
  };

  return _createContact(payload);
}



const addUserToList = async (params) => {
  const rawContact = await _retrieveContact(params);
  const contact = JSON.parse(rawContact || {});

  if(contact && contact.email) {
    const updatePayload = {
      ...params,
      listIds: contact.listIds ? contact.listIds.concat(EMAIL_CONFIG.USER_CONTACT_LIST) : [EMAIL_CONFIG.USER_CONTACT_LIST]
    }

    return _updateContact(updatePayload);
  }

  const payload = {
    ... params,
    listIds: [ EMAIL_CONFIG.USER_CONTACT_LIST ]
  };

  return _createContact(payload);
}


module.exports =  {
  addPartnerToList,
  sendPartnerWelcomeEmail,
  sendUserPartnerWelcomeEmail,
  sendUserWelcomeEmail,
  sendDiscountEmail,
  sendUserInvitationEmail,
  addUserToList
};