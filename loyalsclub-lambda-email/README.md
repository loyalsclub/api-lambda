## Description
This function acts as an interal API for sending EMAILS. It can only be triggered by an SNS Event.

Events available:
  * PARTNER_WELCOME_EMAIL
  * USER_WELCOME_EMAIL
  * ADD_PARTNER_TO_LIST


## For Local Testing

For local testing run:
It is required to have corresponding credentials in a file called config.json on the root folder. Run command on specific lambda folder.

```
$sam build
$sam local invoke -e test/test-event.json -n ../config.json
```

## For Production Deploy

npm run qd -- --parameter-overrides SendinblueApiKey={API_KEY_HERE}

## Credentials required

- SendinBlue API KEY