const AWS = require('aws-sdk');
const { escapeAccents } = require('./utils/string.utils');

const sesV2 = new AWS.SESV2({ region: 'sa-east-1'});

function sendSesEmail({ 
  SENDER_NAME = 'Loyals Club',
  SENDER_EMAIL = 'hello@loyals.club',
  EMAIL_TO,
  REPLY_TO = ['info@loyals.club'],
  TEMPLATE,
  TEMPLATE_DATA,
  CONTACT_LIST_NAME = 'LOYALS_CLUB_USERS',
  TOPIC_NAME = '',
}) {
  const params = {
    FromEmailAddress: `${escapeAccents(SENDER_NAME)} <${SENDER_EMAIL}>`,
    Destination: { 
      ToAddresses: [EMAIL_TO],
    },
    ReplyToAddresses: REPLY_TO,
    Content: {
      Template: {
        TemplateName: TEMPLATE,
        TemplateData: TEMPLATE_DATA,
      }
    },
    ConfigurationSetName: 'loyalsclub-email-cs',
    EmailTags: [
      {
        Name: 'Email',
        Value: TEMPLATE
      },
    ],
    ListManagementOptions: {
      ContactListName: CONTACT_LIST_NAME,
      TopicName: TOPIC_NAME,
    }
  };

  return new Promise((resolve) => {
    sesV2.sendEmail(params, function (err, data) {
      if (err) {
        console.log(err, err.stack);
      } else {
        console.log(data);
        resolve(data);
      }
    });
  })
}

module.exports = { sendSesEmail };