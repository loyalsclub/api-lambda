/** Returns array of objets
 * [{
 * 	message: [String]
 * 	params: [Object] 
 * }]
 */

module.exports = (event) => {
	if (!event || !event.Records || !Array.isArray(event.Records)) return [];
	
	const extractAttributes = MessageAttributes => {
		let attributes = {};
		
		for (let key in MessageAttributes) {
			attributes[key] = MessageAttributes[key].Value
		}

		return attributes;
	}

	const extractMessage = record => {
		if (!record.Sns) return;

		return {
			message: record.Sns.Message,
			params: extractAttributes(record.Sns.MessageAttributes)
		}
	}

	return event.Records.map(extractMessage).filter(message => message);
};
